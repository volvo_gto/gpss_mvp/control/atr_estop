
#include <atr_estop/estop_sub.h>
#include <exception>

using std::placeholders::_1;

namespace atr_estop_sub
{
ATRestop_sub::ATRestop_sub() : Node("estop_subscriber")
{
  estop_Subscriber_ = this->create_subscription<std_msgs::msg::Bool>("estop_flag", 10, std::bind(&ATRestop_sub::Estop_sub_CallBack, this, _1));

  RCLCPP_INFO(rclcpp::get_logger("starting_sub_node"),"E-STOP_SUB NODE started.");
}
ATRestop_sub::~ATRestop_sub()
{
}

// Function for publishing estop data

void ATRestop_sub::Estop_sub_CallBack(std_msgs::msg::Bool estop_Msg_sub_)
{
  		
  //RCLCPP_INFO_STREAM(rclcpp::get_logger("Estop Sub Value"), "GPIORead_SUB:" <<  estop_Msg_sub_.data);
  if(estop_Msg_sub_.data)
  {
    RCLCPP_INFO(get_logger(), "TRUE");
  }
  else
  {
    RCLCPP_INFO(get_logger(), "FALSE");
  }

}



}  // namespace atr_estop_sub

int main(int argc, char* argv[])
{
  try
  {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<atr_estop_sub::ATRestop_sub>());
    rclcpp::shutdown();
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }

  return 0;
}
