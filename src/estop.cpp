
#include <atr_estop/estop.h>
#include <exception>

#define IN  0
#define OUT 1

#define LOW  0
#define HIGH 1

#define PIN  18 /* Physical Pin-12 */

using std::placeholders::_1;

namespace atr_estop
{
ATRestop::ATRestop() : Node("estop_publisher")
{
	estop_Publisher_ = this->create_publisher<std_msgs::msg::Bool>("estop_flag", 10);
  	RCLCPP_INFO(rclcpp::get_logger("starting_node"),"E-STOP NODE started.");
  	//Estop_CallBack();
	timer_= this->create_wall_timer(10ms,std::bind(&ATRestop::Estop_CallBack,this));
}
ATRestop::~ATRestop()
{
}

// Function for publishing estop data

void ATRestop::Estop_CallBack()
{

  	ATRestop::GPIOExport(PIN);
  	ATRestop::GPIODirection(PIN, IN); //18 is pin and 0 is input


  		estop_Msg_.data = ATRestop::GPIORead(PIN); // get data from estop pin 18 of rpi
  		bool Estop_value = ATRestop::GPIORead(PIN);
  		//RCLCPP_INFO_STREAM(rclcpp::get_logger("Estop Value"), "GPIORead_PUB:" << Estop_value);
  		estop_Publisher_->publish(estop_Msg_);


   	ATRestop::GPIOUnexport(PIN);
}

// Function for setting Exporting GPIO pins

int ATRestop::GPIOExport(int pin)
{
#define BUFFER_MAX 3
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
		return(-1);
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}

// Function for setting Unexporting GPIO pins

int ATRestop::GPIOUnexport(int pin)
{
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open unexport for writing!\n");
		return(-1);
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}

// Function for setting Direction of GPIO pins (Input / Output Pins)

int ATRestop::GPIODirection(int pin, int dir)
{
	static const char s_directions_str[]  = "in\0out";

#define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;

	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio direction for writing!\n");
		return(-1);
	}

	if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3)) {
		fprintf(stderr, "Failed to set direction!\n");
		return(-1);
	}

	close(fd);
	return(0);
}

// Function for Reading GPIO pins

int ATRestop::GPIORead(int pin)
{
#define VALUE_MAX 30
	char path[VALUE_MAX];
	char value_str[3];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_RDONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for reading!\n");
		return(-1);
	}

	if (-1 == read(fd, value_str, 3)) {
		fprintf(stderr, "Failed to read value!\n");
		return(-1);
	}

	close(fd);

	return(atoi(value_str));
}

// Function for Writing GPIO pins

int ATRestop::GPIOWrite(int pin)
{
	char path[VALUE_MAX];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for writing!\n");
		return(-1);
	}

	close(fd);
	return(0);
}

}  // namespace atr_estop

int main(int argc, char* argv[])
{
  try
  {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<atr_estop::ATRestop>());
    rclcpp::shutdown();
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }

  return 0;
}

/*
int
main(int argc, char *argv[])
{
	
	  //Enable GPIO pins
	 
	if (-1 == GPIOExport(POUT) || -1 == GPIOExport(PIN))
		return(1);

	
	 //Set GPIO directions
	 
	if (-1 == GPIODirection(POUT, OUT) || -1 == GPIODirection(PIN, IN))
		return(2);

	while(1)
	{
		//Write GPIO value
		 
		if (-1 == GPIOWrite(POUT))
			return(3);

		// Read GPIO value
		 
		printf("I'm reading %d in GPIO %d\n", GPIORead(PIN), PIN);

		usleep(500 * 1000);
	}

	// Disable GPIO pins
	
	if (-1 == GPIOUnexport(POUT) || -1 == GPIOUnexport(PIN))
		return(4);

	return(0);
}
*/