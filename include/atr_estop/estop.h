#ifndef ESTOP_H
#define ESTOP_H

#include "rclcpp/rclcpp.hpp"
#include <sensor_msgs/msg/joy.hpp>
#include <std_msgs/msg/bool.hpp>
#include <geometry_msgs/msg/twist_stamped.hpp>
#include <boost/shared_ptr.hpp>


#include <string>
#include <vector>
#include <math.h>

#include "time.h"
#include "stdlib.h"
#include <memory>
#include <chrono>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std::literals::chrono_literals;

namespace atr_estop
{


class ATRestop : public rclcpp::Node
{

private:
  rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr estop_Publisher_;  
  rclcpp::TimerBase::SharedPtr timer_;
  std_msgs::msg::Bool estop_Msg_;  ///< bool message

  // public methods
public:

  ATRestop();


  ~ATRestop();
    
    int GPIOExport(int pin);
    int GPIOUnexport(int pin);
    int GPIODirection(int pin, int dir);
    int GPIORead(int pin);
    int GPIOWrite(int pin);

  void Estop_CallBack();


  // private methods
private:

 

};


}  // namespace atr_estop

#endif