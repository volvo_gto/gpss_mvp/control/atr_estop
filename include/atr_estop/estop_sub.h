#ifndef ESTOP_H
#define ESTOP_H

#include "rclcpp/rclcpp.hpp"
#include <sensor_msgs/msg/joy.hpp>
#include <std_msgs/msg/bool.hpp>
#include <geometry_msgs/msg/twist_stamped.hpp>
#include <boost/shared_ptr.hpp>

#include <string>
#include <vector>
#include <math.h>

#include "time.h"
#include "stdlib.h"
#include <memory>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <boost/shared_ptr.hpp>

namespace atr_estop_sub
{


class ATRestop_sub : public rclcpp::Node
{

private:
  rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr estop_Subscriber_;  

  std_msgs::msg::Bool estop_Msg_sub_;  ///< bool message

  // public methods
public:

  ATRestop_sub();


  ~ATRestop_sub();
    

  void Estop_sub_CallBack(std_msgs::msg::Bool estop_Msg_sub_);


  // private methods
private:

 

};


}  // namespace atr_estop

#endif