import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.conditions import IfCondition
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

from launch.actions import IncludeLaunchDescription
from launch.actions import GroupAction
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import TextSubstitution
from launch_ros.actions import PushRosNamespace

# How to run:
# Default parameters
# ros2 launch atr_estop1 estop_launch.launch.py



def generate_launch_description():
    # Create the launch description and populate
    ld = LaunchDescription()

    # Get the launch directory
    bringup_dir = get_package_share_directory("atr_estop1")

    # args that can be set from the command line or a default will be used
    dev_name_launch_arg = DeclareLaunchArgument("_dev", default_value="/dev/input/js0")
    ld.add_action(dev_name_launch_arg)
    deadzone_launch_arg = DeclareLaunchArgument("_deadzone", default_value=TextSubstitution(text="0.05"))
    ld.add_action(deadzone_launch_arg)
    autorepeat_launch_arg = DeclareLaunchArgument("_autorepeat_rate", default_value=TextSubstitution(text="30.0"))
    ld.add_action(autorepeat_launch_arg)

    # -------- NODES

    # estop node

  

    estop_pub = Node(
        package="atr_estop",
        name="estop_publisher",
        executable="estop",
        output="screen",
    )
    ld.add_action(estop_pub)

    estop_sub = Node(
        package="atr_estop_sub",
        name="estop_subscriber",
        executable="estop_sub",
        output="screen",
    )
    ld.add_action(estop_sub)

    return ld
