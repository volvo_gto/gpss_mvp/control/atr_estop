# atr_estop

This repo contains a ros2 package with the ATR E-Stop. It provides the stopping of ATR when physically-mounted E-stop button on ATR is pressed.

## Dependencies

---

The dependencies of this package are listed in the  package file (<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_estop/-/blob/main/package.xml>)

## Description

---

This package provides the the stopping of ATR when physically-mounted E-stop button on ATR is pressed. The pin for connecting the E-Stop switch is GPIO 18 (Physical Pin 12) and 3.3v DC Power pin (Physical Pin 1). 

### Input

The input of this node is the hardware (Raspberry Pi 4) on the ATR to get the E-Stop button press event as **Bool** message (std_msg/msg/Bool). 

### Output

This node produces outputs as "TRUE" OR "FALSE" based on the physically-mounted E-stop button press event.

## TODO

---

For the moment, this node only provides the E-Stop button press event. This ros package will be ported and upgraded to include communication to the ATR hardware (atr_driver2_hw) and send wheel velocities to 0.0 when the E-Stop button is pressed.

## How to run the node

---

To launch the node following steps are to be followed:

1. sudo -s                                          (for accessing the RPI 4 Hardware).
2. ros2 launch atr_estop estop.launch.py            (launching the atr_estop node).
